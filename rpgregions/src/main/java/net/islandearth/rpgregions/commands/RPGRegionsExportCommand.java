package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.effects.RegionEffect;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.logging.Level;

@CommandAlias("rpgre")
@CommandPermission("rpgregions.export")
public class RPGRegionsExportCommand extends BaseCommand {

    private final RPGRegions plugin;

    public RPGRegionsExportCommand(RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Use this command to export/import a region template.");
    }

    @HelpCommand
    @Subcommand("help")
    public void onHelp(final CommandHelp commandHelp) {
        commandHelp.showHelp();
    }

    @Subcommand("export")
    @CommandCompletion("@regions")
    public void onExport(CommandSender sender, ConfiguredRegion region) {
        File templates = new File(plugin.getDataFolder() + File.separator + "templates");
        File target = new File(templates + File.separator + region.getId() + "_template.json");
        region.save(plugin, target);

        sender.sendMessage(ChatColor.GREEN + "Your region has been saved as a template to " + target + ".");
        sender.sendMessage(ChatColor.YELLOW + "You may use /rpgre import " + region.getId() + " <target> to import the requirements, rewards, etc. of this template to the target region.");
    }

    @Subcommand("import")
    @CommandCompletion("@templates @regions")
    public void onImport(CommandSender sender, String template, ConfiguredRegion region) {
        File templateFile = new File(plugin.getDataFolder() + File.separator + "templates" + File.separator + template);
        if (!templateFile.exists()) {
            sender.sendMessage(ChatColor.RED + "That template does not exist.");
            return;
        }

        int rewards = 0, requirements = 0, effects = 0;
        sender.sendMessage(ChatColor.GREEN + "Reading template file...");
        try (Reader reader = new FileReader(templateFile)) {
            ConfiguredRegion templateRegion = plugin.getGson().fromJson(reader, ConfiguredRegion.class);
            sender.sendMessage(ChatColor.GREEN + "Converting rewards to " + region.getId() + "...");
            for (DiscoveryReward reward : templateRegion.getRewards()) {
                region.getRewards().add(reward);
                rewards++;
            }

            sender.sendMessage(ChatColor.GREEN + "Converting requirements to " + region.getId() + "...");
            for (RegionRequirement requirement : templateRegion.getRequirements()) {
                region.getRequirements().add(requirement);
                requirements++;
            }

            sender.sendMessage(ChatColor.GREEN + "Converting effects to " + region.getId() + "...");
            for (RegionEffect effect : templateRegion.getEffects()) {
                region.getEffects().add(effect);
                effects++;
            }
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Error loading template config " + templateFile.getName() + ".", e);
        }

        sender.sendMessage(ChatColor.GREEN + String.format("Done transferring (%d, %d, %d) rewards, requirements, and effects.", rewards, requirements, effects));
    }
}
