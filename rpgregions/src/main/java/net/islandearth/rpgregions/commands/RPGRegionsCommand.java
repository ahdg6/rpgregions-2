package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.api.events.RPGRegionsReloadEvent;
import net.islandearth.rpgregions.api.integrations.IntegrationType;
import net.islandearth.rpgregions.gui.DiscoveryGUI;
import net.islandearth.rpgregions.gui.RegionCreateGUI;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.regenerate.Regenerate;
import net.islandearth.rpgregions.rewards.ItemReward;
import net.islandearth.rpgregions.utils.RegenUtils;
import net.islandearth.rpgregions.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@CommandAlias("rpgregions|rpgr")
public class RPGRegionsCommand extends BaseCommand {

    private final RPGRegions plugin;
    private final List<UUID> regenerateConfirm = new ArrayList<>();
    private static final String WARNING_MESSAGE = ChatColor.RED + "" + ChatColor.ITALIC + "" + ChatColor.BOLD
            + "The regenerate configuration is very dangerous and can delete world sections if used wrongly.";

    public RPGRegionsCommand(RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandSender sender) {
        if (sender.hasPermission("rpgregions.noview") && !sender.isOp()) return;

        sender.sendMessage(ChatColor.YELLOW + "Wiki > " + plugin.getDescription().getWebsite());
        sender.sendMessage(ChatColor.YELLOW + "Bugs > https://gitlab.com/SamB440/rpgregions-2");
        sender.sendMessage(ChatColor.YELLOW + "Discord > https://discord.gg/fh62mxU");
        int rewards = 0;
        for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
            rewards += configuredRegion.getRewards().size();
        }
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "" +
                plugin.getManagers().getRegionsCache().getConfiguredRegions().size()
                + " regions are loaded with " + rewards + " rewards.");
    }

    @HelpCommand
    @Subcommand("help")
    public void onHelp(final CommandHelp commandHelp) {
        commandHelp.showHelp();
    }

    @Subcommand("about")
    public void onAbout(CommandSender sender) {
        sender.sendMessage(StringUtils.colour("&eRPGRegions v" + plugin.getDescription().getVersion() + "."));
        sender.sendMessage(StringUtils.colour("&eOwner: https://www.spigotmc.org/members/%%__USER__%%/"));
        sender.sendMessage(StringUtils.colour("&eStorage: " + plugin.getManagers().getStorageManager().getClass().getName()));
        sender.sendMessage(StringUtils.colour("&eIntegration: " + plugin.getManagers().getIntegrationManager().getClass().getName()));
    }

    @Subcommand("add")
    @CommandPermission("rpgregions.add")
    @CommandCompletion("@integration-regions")
    public void onAdd(CommandSender sender, String region, @co.aikar.commands.annotation.Optional @Nullable String worldName) {
        if (plugin.getManagers().getRegionsCache().getConfiguredRegion(region).isPresent()) {
            sender.sendMessage(StringUtils.colour("&cThat region is already configured."));
            return;
        }

        Location location = null;
        if (sender instanceof Player player) {
            location = player.getLocation();
        }

        final World world = worldName == null ? null : Bukkit.getWorld(worldName);
        if (world != null) {
            if (location == null) location = new Location(world, 0, 100, 0);
            else location.setWorld(world);
        }

        if (location == null) {
            sender.sendMessage(ChatColor.RED + "Console needs to provide a world name!");
            return;
        }

        if (!plugin.getManagers().getIntegrationManager().exists(location.getWorld(), region)) {
            sender.sendMessage(StringUtils.colour("&cThat region does not exist in your protection plugin."));
            return;
        }

        add(location, region);
        sender.sendMessage(StringUtils.colour("&aAdded configured region " + region + "!"));
        sender.sendMessage(StringUtils.colour("&e&oNow use /rpgregions edit "
                + region
                + " to edit it!"));
        sender.sendMessage(StringUtils.colour("&e&oUse /rpgregions save to save this to file for editing."));
    }

    private void add(@NotNull final Location location, final String region) {
        ConfiguredRegion configuredRegion = new ConfiguredRegion(location.getWorld(), region, region, new ArrayList<>(),
                new ArrayList<>());
        configuredRegion.setLocation(location);
        plugin.getManagers().getRegionsCache().addConfiguredRegion(configuredRegion);
    }

    @Subcommand("setname")
    @CommandPermission("rpgregions.setname")
    @CommandCompletion("@regions")
    public void onSetName(CommandSender sender, String region, String regionName) {
        Optional<ConfiguredRegion> configuredRegion = plugin.getManagers().getRegionsCache().getConfiguredRegion(region);
        if (configuredRegion.isEmpty()) {
            sender.sendMessage(StringUtils.colour("&cRegion '" + region + "' does not exist yet, create it first."));
            return;
        }
        configuredRegion.get().setCustomName(regionName);
        sender.sendMessage(StringUtils.colour("&aSet name of region '" + region + "' to: " + regionName));
    }

    @Subcommand("remove")
    @CommandPermission("rpgregions.remove")
    @CommandCompletion("@regions")
    public void onRemove(CommandSender sender, ConfiguredRegion configuredRegion) {
        if (configuredRegion != null) {
            configuredRegion.delete(plugin);
            plugin.getManagers().getRegionsCache().removeConfiguredRegion(configuredRegion.getId());
            sender.sendMessage(StringUtils.colour("&cRemoved configured region " + configuredRegion.getId() + "!"));
        } else {
            sender.sendMessage(StringUtils.colour("&cA region by that name is not yet configured."));
        }
    }

    @Subcommand("edit")
    @CommandPermission("rpgregions.edit")
    @CommandCompletion("@regions")
    public void onEdit(Player player, ConfiguredRegion configuredRegion) {
        new RegionCreateGUI(plugin, player, configuredRegion).open();
    }

    @Subcommand("list|discoveries")
    @CommandPermission("rpgregions.list")
    public void onList(Player player) {
        new DiscoveryGUI(plugin, player).open();
    }

    @Subcommand("additem")
    @CommandPermission("rpgregions.additem")
    @CommandCompletion("@regions")
    public void onAddItem(Player player, ConfiguredRegion configuredRegion) {
        if (configuredRegion != null) {
            configuredRegion.getRewards().add(new ItemReward(plugin, player.getInventory().getItemInMainHand()));
            player.sendMessage(ChatColor.GREEN + "Item added to configuration!");
        } else {
            player.sendMessage(ChatColor.RED + "No region exists by that name.");
        }
    }

    @Subcommand("reload")
    @CommandPermission("rpgregions.reload")
    public void onReload(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Reloading region files...");
        long startTime = System.currentTimeMillis();
        File folder = new File(plugin.getDataFolder() + "/regions/");
        plugin.getManagers().getRegionsCache().clear();

        for (File file : folder.listFiles()) {
            // Exclude non-json files
            if (file.getName().endsWith(".json")) {
                try (Reader reader = new FileReader(file)) {
                    ConfiguredRegion region = plugin.getGson().fromJson(reader, ConfiguredRegion.class);
                    if (!region.getId().equals("exampleconfig"))
                        plugin.getManagers().getRegionsCache().addConfiguredRegion(region);
                } catch (Exception e) {
                    plugin.getLogger().severe("Error loading region config " + file.getName() + ":");
                    e.printStackTrace();
                }
            }
        }

        plugin.reloadConfig();
        plugin.getManagers().getRegenerationManager().reload();
        Bukkit.getPluginManager().callEvent(new RPGRegionsReloadEvent());
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        sender.sendMessage(ChatColor.GREEN + "Done! (" + totalTime + "ms)");
    }

    @Subcommand("save")
    @CommandPermission("rpgregions.save")
    @CommandCompletion("@async")
    public void onSave(CommandSender sender, @co.aikar.commands.annotation.Optional String[] args) {
        boolean async = Arrays.asList(args).contains("--async");
        sender.sendMessage(ChatColor.GREEN + "Saving data..." + (async ? ChatColor.GOLD + " (async)" : ""));
        long startTime = System.currentTimeMillis();

        // Save all player data (quit event not called for shutdown)
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
                plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
        });

        // Save all region configs
        long asyncStartTime = System.currentTimeMillis();
        CompletableFuture<Boolean> saveFuture = plugin.getManagers().getRegionsCache().saveAll(async);
        long mainEndTime = System.currentTimeMillis();
        long mainTotalTime = mainEndTime - startTime;
        saveFuture.thenAccept(saved -> {
           long asyncTotalTime = System.currentTimeMillis() - asyncStartTime;
           StringBuilder sb = new StringBuilder();
           sb.append(ChatColor.GREEN).append("Done! (%sms)".formatted(mainTotalTime));
           if (async) sb.append(' ').append(ChatColor.GOLD).append("(async execution took %sms)".formatted(asyncTotalTime));
           sender.sendMessage(sb.toString());
        });
    }

    @Subcommand("reset")
    @CommandPermission("rpgregions.reset")
    @CommandCompletion("@players @regions")
    public void onReset(CommandSender sender, String[] args) {
        @SuppressWarnings({"deprecation"}) // We know what we're doing.
        OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
        if (!player.hasPlayedBefore()) {
            sender.sendMessage(ChatColor.RED + "That player cannot be found.");
            return;
        }

        switch (args.length) {
            case 1 -> {
                plugin.getManagers().getStorageManager().clearDiscoveries(player.getUniqueId());
                if (!player.isOnline())
                    plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
                sender.sendMessage(ChatColor.GREEN + "Players discoveries has been cleared.");
            }
            case 2 -> {
                String regionId = args[1];
                if (plugin.getManagers().getRegionsCache().getConfiguredRegions().containsKey(regionId)) {
                    plugin.getManagers().getStorageManager().clearDiscovery(player.getUniqueId(), regionId);
                    if (!player.isOnline())
                        plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
                    sender.sendMessage(ChatColor.GREEN + "Discovery cleared.");
                } else {
                    sender.sendMessage(ChatColor.RED + "Region does not exist or is not configured.");
                }
            }
            default -> sender.sendMessage(ChatColor.RED + "Usage: /rpgregions reset <player> [region_id]");
        }
    }

    @Subcommand("delete")
    @CommandPermission("rpgregions.delete")
    @CommandCompletion("@players")
    public void onDelete(CommandSender sender, Player player) {
        if (player != null) {
            plugin.getManagers().getStorageManager().deleteAccount(player.getUniqueId());
            sender.sendMessage(ChatColor.GREEN + "Deleted account of player.");
        } else {
            sender.sendMessage(ChatColor.RED + "That player cannot be found.");
        }
    }

    @Subcommand("setlocation")
    @CommandPermission("rpgregions.setlocation")
    @CommandCompletion("@regions")
    public void onSetLocation(Player player, ConfiguredRegion configuredRegion) {
        if (configuredRegion != null) {
            Location location = player.getLocation();
            configuredRegion.setLocation(location);
            player.sendMessage(ChatColor.GREEN + "Location has been updated.");
        } else {
            player.sendMessage(StringUtils.colour("&cA region by that name is not yet configured."));
        }
    }

    @Subcommand("regenerate|regen")
    @CommandPermission("rpgregions.regenerate")
    @CommandCompletion("@regions")
    public void onRegenerate(Player player, ConfiguredRegion configuredRegion) {
        IntegrationType integrationType = IntegrationType.valueOf(plugin.getConfig().getString("settings.integration.name").toUpperCase());
        if (integrationType != IntegrationType.WORLDGUARD) {
            player.sendMessage(ChatColor.RED + "Regeneration only supports WorldGuard integrations.");
            return;
        }

        if (!regenerateConfirm.contains(player.getUniqueId())) {
            regenerateConfirm.add(player.getUniqueId());
            player.sendMessage(ChatColor.YELLOW + "Run /rpgregions regenerate " + configuredRegion.getId() + " again to confirm. Only use if you know what you are doing!");
            player.sendMessage(WARNING_MESSAGE);
        } else {
            regenerateConfirm.remove(player.getUniqueId());
            player.sendMessage(ChatColor.GREEN + "Regenerating region...");
            long startTime = System.currentTimeMillis();
            boolean done = RegenUtils.regenerate(configuredRegion);
            if (!done) {
                player.sendMessage(ChatColor.RED + "Unable to regenerate region. Check console for details.");
            }
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;
            player.sendMessage(ChatColor.GREEN + "Done! (" + totalTime + "ms)");
        }
    }

    @Subcommand("setschematic")
    @CommandPermission("rpgregions.setschematic")
    @CommandCompletion("@regions @schematics @nothing")
    public void onAddSchematic(Player player, ConfiguredRegion configuredRegion, String schematicName) {
        IntegrationType integrationType = IntegrationType.valueOf(plugin.getConfig().getString("settings.integration.name").toUpperCase());
        if (integrationType != IntegrationType.WORLDGUARD) {
            player.sendMessage(ChatColor.RED + "Regeneration only supports WorldGuard integrations.");
            return;
        }

        if (configuredRegion != null) {
            if (!regenerateConfirm.contains(player.getUniqueId())) {
                regenerateConfirm.add(player.getUniqueId());
                player.sendMessage(ChatColor.YELLOW + "Run /rpgregions addschematic " + schematicName + " again to confirm. Only use if you know what you are doing!");
                player.sendMessage(ChatColor.RED + "MAKE SURE YOU ARE STANDING WHERE YOU CREATED THE SCHEMATIC ORIGINALLY (//copy, //schematic save), OTHERWISE IT WILL NOT PASTE CORRECTLY.");
            } else {
                regenerateConfirm.remove(player.getUniqueId());
                Regenerate regenerate = configuredRegion.getRegenerate();
                if (regenerate == null) regenerate = new Regenerate(Integer.MAX_VALUE, false, new ArrayList<>());
                regenerate.setSchematicName(schematicName);
                regenerate.setOrigin(player.getLocation());
                configuredRegion.setRegenerate(regenerate);
                player.sendMessage(ChatColor.GREEN + "This region has had a regenerate section added, and schematicName set to " + schematicName + " and origin set to " + player.getLocation() + ".");
                player.sendMessage(ChatColor.YELLOW + "Run /rpgregions save and " + ChatColor.BOLD + "CONFIGURE BEFORE RELOADING OR RESTARTING THE SERVER.");
            }
            player.sendMessage(WARNING_MESSAGE);
        }
    }

    @Subcommand("forceupdateicons")
    @CommandPermission("rpgregions.forceupdate")
    public void onForceUpdateIcons(Player player) {
        Optional<Material> defaultIcon = Optional.of(Material.valueOf(RPGRegionsAPI.getAPI().getConfig().getString("settings.server.gui.default_region_icon")));
        defaultIcon.ifPresent(xMaterial -> {
            plugin.getManagers().getRegionsCache().getConfiguredRegions().forEach((name, region) -> {
                region.setUndiscoveredIcon(xMaterial);
                region.setIcon(xMaterial);
                player.sendMessage(ChatColor.GREEN + "Updated icons for: " + name + ".");
            });
        });
    }
}
