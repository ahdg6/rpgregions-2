package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.integrations.rpgregions.RPGRegionsIntegration;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.CuboidRegion;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.PolyRegion;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommandAlias("rpgri|rpgrintegration")
@CommandPermission("rpgregions.integration")
public class RPGRegionsIntegrationCommand extends BaseCommand {

    private final RPGRegions plugin;

    public RPGRegionsIntegrationCommand(final RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "RPGRegions region integration is enabled. Type /rpgri help for help.");
    }

    @HelpCommand
    @Subcommand("help")
    public void onHelp(final CommandHelp commandHelp) {
        commandHelp.showHelp();
    }

    @Subcommand("list")
    public void onList(final CommandSender sender) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.getAllRegionNames(null).forEach(name -> sender.sendMessage(ChatColor.GREEN + "- " + name));
    }

    @Subcommand("info")
    @CommandCompletion("@integration-regions")
    public void onInfo(final CommandSender sender, final String name) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        Optional<RPGRegionsRegion> region = integration.getRegion(name);
        if (region.isPresent()) {
            sender.sendMessage(ChatColor.GREEN + "Name: " + ChatColor.WHITE + region.get().getName());
            sender.sendMessage(ChatColor.GREEN + "Priority: " + ChatColor.WHITE + region.get().getPriority());
            sender.sendMessage(ChatColor.GREEN + "Points for " + region.get().getName() + ":");
            region.get().getPoints().forEach(point -> sender.sendMessage(" " + point.toString()));
            return;
        }
        sender.sendMessage(ChatColor.RED + "Region " + name + " does not exist!");
    }

    @Subcommand("save")
    public void onSave(final CommandSender sender) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.save();
        sender.sendMessage(ChatColor.GREEN + "Regions saved.");
    }

    @Subcommand("create")
    @CommandCompletion("@integration-regions @region-types @worlds")
    public void onCreate(final CommandSender sender, final String name, final String regionType,
                         @co.aikar.commands.annotation.Optional @Nullable String worldName) {
        // If worldName is null, try to get from sender if they are a player, else overworld, else worldName world
        World world = worldName == null ? sender instanceof Player player ? player.getWorld() : Bukkit.getWorlds().get(0) : Bukkit.getWorld(worldName);
        RPGRegionsRegion region = switch (regionType.toLowerCase(Locale.ENGLISH)) {
            case "cuboid" -> new CuboidRegion(name, world);
            case "poly" -> new PolyRegion(name, world);
            default -> null;
        };

        if (region == null) {
            sender.sendMessage(ChatColor.RED + "A region of the type " + regionType + " cannot be found!");
            return;
        }

        if (worldName == null && !(sender instanceof Player)) {
            sender.sendMessage(ChatColor.YELLOW + "WARNING: World name was not provided and you are not a player. Defaulted to '" + world.getName() + "'.");
        }

        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.addRegion(region);
        sender.sendMessage(ChatColor.GREEN + "Created region " + name + ".");
    }

    @Subcommand("delete")
    @CommandCompletion("@integration-regions")
    public void onDelete(final CommandSender sender, final RPGRegionsRegion region) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.removeRegion(region);
        sender.sendMessage(ChatColor.GREEN + "Region " + region.getName() + " has been removed.");
    }

    @Subcommand("addpos")
    @CommandCompletion("@integration-regions")
    public void onAddPos(final CommandSender sender, final RPGRegionsRegion region,
                         @co.aikar.commands.annotation.Optional World world,
                         @co.aikar.commands.annotation.Optional Double x,
                         @co.aikar.commands.annotation.Optional Double y,
                         @co.aikar.commands.annotation.Optional Double z) {
        System.out.println("world: " + world + ", x:" + x + ", y:" + y + ", z:" + z);
        if (sender instanceof Player player && x == null && y == null && z == null) {
            if (!region.addPoint(player.getLocation())) {
                player.sendMessage(ChatColor.RED + "Could not add a point to that region because it exceeds the size for its type.");
            } else {
                player.sendMessage(ChatColor.GREEN + "Added point to " + region.getName() + ".");
            }
        } else {
            if (world == null || x == null || y == null || z == null) {
                sender.sendMessage(ChatColor.RED + "You need to specify the world, x, y, z of the point.");
                return;
            }

            Location location = new Location(world, x, y, z);
            region.addPoint(location);
            sender.sendMessage(ChatColor.GREEN + "Added point to " + region.getName() + ".");
        }
    }

    @Subcommand("setpriority")
    @CommandCompletion("@integration-regions @range:20")
    public void onSetPriority(final Player player, final RPGRegionsRegion region, final int priority) {
        region.setPriority(priority);
        player.sendMessage(ChatColor.GREEN + "Set priority of " + region.getName() + " to " + priority + ".");
    }

    @Subcommand("setworld")
    @CommandCompletion("@integration-regions @worlds")
    public void onSetWorld(final CommandSender sender, final RPGRegionsRegion region, final String worldName) {
        final World world = Bukkit.getWorld(worldName);
        if (world == null) {
            sender.sendMessage(ChatColor.RED + "That world could not be found.");
            return;
        }
        region.setWorld(world.getUID());
        sender.sendMessage(ChatColor.GREEN + "Set region '" + region.getName() + "' world to '" + world.getName() + "'.");
    }

    @Subcommand("visualise")
    @CommandCompletion("@integration-regions")
    public void onVisualise(final Player sender, final RPGRegionsRegion region) {
        region.visualise(sender);
    }

    @Subcommand("whereami")
    public void onWhereAmI(final Player sender) {
        final RPGRegionsIntegration manager = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        List<RPGRegionsRegion> regions = new ArrayList<>();
        for (RPGRegionsRegion region : manager.getRegions()) {
            if (region.isWithinBounds(sender)) {
                regions.add(region);
            }
        }

        if (regions.isEmpty()) {
            sender.sendMessage(ChatColor.RED + "You are not currently inside any region.");
        } else {
            sender.sendMessage(ChatColor.GREEN + "You are currently inside these regions:");
            for (RPGRegionsRegion region : regions) {
                sender.sendMessage(ChatColor.GREEN + " - " + region.getName() + " (p: " + region.getPriority() + ")");
            }
        }
    }

    @Subcommand("migrate")
    @CommandCompletion("@worlds")
    public void onMigrate(final CommandSender sender, final String worldName) {
        final World world = Bukkit.getWorld(worldName);
        if (world == null) {
            sender.sendMessage(ChatColor.RED + "That world could not be found.");
            return;
        }

        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        for (RPGRegionsRegion region : integration.getRegions()) {
            region.setWorld(world.getUID());
        }

        sender.sendMessage(ChatColor.GREEN + "Set all regions to world '" + worldName + "'.");
    }
}
