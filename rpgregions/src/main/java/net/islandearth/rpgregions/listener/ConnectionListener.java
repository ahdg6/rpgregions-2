package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public record ConnectionListener(RPGRegions plugin) implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (!plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId())) {
            player.kickPlayer(ChatColor.RED + "Player user data not present! Please contact an administrator.");
        }
    }

    // PlayerLoginEvent is called AFTER AsyncPlayerPreLoginEvent
    // We can't really avoid loading the user first unfortunately
    @EventHandler(priority = EventPriority.HIGHEST) // Highest so we are always the last called
    public void onValidate(final PlayerLoginEvent event) {
        if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) {
            // DO NOT save the user if this is due to whitelist!
            // If they are a new user that was prevented from joining due to whitelist
            // Then when they join after whitelist is disabled, they will not be considered new
            // Also saves performance
            plugin.getManagers().getStorageManager().removeCachedAccount(event.getPlayer().getUniqueId(), false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST) // Highest so we are always the last called
    public void onJoin(AsyncPlayerPreLoginEvent event) {
        // If something else has prevented the player from joining, we don't want to load the user.
        // Otherwise, we will get memory leaks.
        if (event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) return;

        UUID uuid = event.getUniqueId();

        // If the user is present then we still haven't saved, prevent them joining to avoid corrupt data
        if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(uuid)) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.YELLOW + "You're rejoining too quickly! Give us a moment to save your data.");
            return;
        }

        try {
            RPGRegionsAccount account = plugin.getManagers().getStorageManager().getAccount(uuid).get();
            if (account != null) return;
        } catch (Exception e) {
            // If there was an error, don't allow entry!
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "An error occurred whilst loading player user data! Please contact an administrator.");
            e.printStackTrace();
        }

        event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "RPGRegions account could not be loaded.");
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent pqe) {
        Player player = pqe.getPlayer();
        if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
            plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
    }
}
