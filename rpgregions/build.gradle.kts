repositories {
    maven("https://betonquest.org/nexus/repository/betonquest/")
    maven("https://nexus.phoenixdvpt.fr/repository/maven-public/")
    maven("https://repo.ryandw11.com/repository/maven-releases/")
}

dependencies {
    implementation(project(":api"))

    testImplementation("junit:junit:4.13.2")
    testImplementation("com.github.seeseemelk:MockBukkit-v1.17:1.13.0")
    testImplementation("org.reflections:reflections:0.10.2")

    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("net.wesjd:anvilgui:1.6.3-SNAPSHOT") // anvilgui
    implementation("com.github.stefvanschie.inventoryframework:IF:0.10.8") // inventory framework
    implementation("co.aikar:acf-paper:0.5.1-SNAPSHOT") // commands
    implementation("co.aikar:idb-core:1.0.0-SNAPSHOT") // database
    implementation("org.bstats:bstats-bukkit:3.0.0") // plugin stats
    implementation("io.papermc:paperlib:1.0.7") // paperlib - async teleport on Paper

    compileOnly("com.sk89q.worldguard:worldguard-bukkit:7.0.4-SNAPSHOT") {
        exclude("com.destroystokyo.paper")
        exclude("org.spigotmc")
    }
    compileOnly("com.sk89q.worldedit:worldedit-bukkit:7.2.0-SNAPSHOT") {
        exclude("com.google")
        exclude("org.bukkit")
        exclude("org.spigotmc")
    }
    //compileOnly 'com.zaxxer:HikariCP:2.4.1' // IMPLEMENTED VIA LIBRARIES - database
    compileOnly("me.clip:placeholderapi:2.10.4") // PAPI
    compileOnly("com.github.MilkBowl:VaultAPI:1.7") { // vault
        exclude("org.bukkit")
    }
    compileOnly(":AlonsoLevels_v2.0-BETA") // alonsolevels
    compileOnly(":Quests-4.0.1") // quests
    compileOnly("pl.betoncraft:betonquest:1.12.0")
    compileOnly("net.Indyuce:MMOCore-API:1.9.5-SNAPSHOT")
    compileOnly("com.github.shynixn.headdatabase:hdb-api:1.0") // head database
    compileOnly("com.github.plan-player-analytics:Plan:5.4.1366") // plan
    compileOnly("io.lumine.xikage:MythicMobs:4.9.1") {
        exclude("org.apache.commons")
    }
    compileOnly(":Dynmap-3.1-spigot") // Dynmap
    compileOnly("com.comphenix.protocol:ProtocolLib:5.0.0-SNAPSHOT")
    compileOnly("com.ryandw11:CustomStructures:1.8.2")
}

configurations.all {
    exclude("commons-io")
    exclude("commons-codec")
}

tasks {
    javadoc {
        exclude("net/islandearth/rpgregions/translation/**")
        exclude("net/islandearth/rpgregions/listener/**")
        exclude("net/islandearth/rpgregions/gson/**")
        exclude("net/islandearth/rpgregions/commands/**")
        exclude("net/islandearth/rpgregions/utils/**")
    }

    shadowJar {
        relocate("co.aikar.commands", "net.islandearth.rpgregions.libs.acf")
        relocate("co.aikar.locales", "net.islandearth.rpgregions.libs.acf.locales")
        relocate("co.aikar.idb", "net.islandearth.rpgregions.libs.idb")
        relocate("com.github.stefvanschie.inventoryframework", "net.islandearth.rpgregions.libs.inventoryframework")
        relocate("org.bstats", "net.islandearth.rpgregions.libs.bstats")
        relocate("net.wesjd", "net.islandearth.rpgregions.libs.anvilgui")
    }
}