package net.islandearth.rpgregions.api.events;

import net.islandearth.rpgregions.managers.data.region.Discovery;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

public class RegionDiscoverEvent extends Event {

	private static final HandlerList HANDLER_LIST = new HandlerList();
	private final Player player;
	private final String region;
	private final Discovery discovery;
	
	public RegionDiscoverEvent(Player player, String region, Discovery discovery) {
		this.player = player;
		this.region = region;
		this.discovery = discovery;
	}

	/**
	 * The player involved in this event.
	 * @return the player involved
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets the region that has been discovered.
	 * @return {@link List} of regions
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Gets the discovery involved. Contains useful information such as the date.
	 * @return the region {@link Discovery}
	 */
	public Discovery getDiscovery() {
		return discovery;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLER_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLER_LIST;
	}

}