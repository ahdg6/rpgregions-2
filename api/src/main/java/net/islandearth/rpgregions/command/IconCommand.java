package net.islandearth.rpgregions.command;

public class IconCommand {

    private final String command;
    private final CommandClickType clickType;
    private final int cooldown;

    public IconCommand(String command, CommandClickType clickType, int cooldown) {
        this.command = command;
        this.clickType = clickType;
        this.cooldown = cooldown;
    }

    public String getCommand() {
        return command;
    }

    public CommandClickType getClickType() {
        return clickType;
    }
    
    public int getCooldown() {
        return cooldown;
    }
    
    public enum CommandClickType {
        DISCOVERED,
        UNDISCOVERED
    }
}
