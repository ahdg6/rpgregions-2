package net.islandearth.rpgregions.utils;

import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Locale;

public class TitleAnimator {

    public TitleAnimator(Player player, IRPGRegionsAPI plugin, List<String> titles, List<String> subtitles, int speed) {
        new BukkitRunnable() {
            int current = 0;
            @Override
            public void run() {
                String title = current < titles.size() ? titles.get(current) : "";
                String subtitle = current < subtitles.size() ? subtitles.get(current) : "";
                plugin.debug("Title is: " + title + " Subtitle is: " + subtitle);
                if (current >= titles.size()
                        && current >= subtitles.size()) {
                    plugin.debug("Cancelling! No more titles left to send.");
                    this.cancel();
                    return;
                }

                plugin.debug("Successful title send!");
                int fadein = plugin.getConfig().getInt("settings.server.discoveries.discovered.title.fadein");
                if (current >= 1) {
                    fadein = 0;
                    final String animationSound = plugin.getConfig().getString("settings.server.discoveries.discovered.title.animation_sound", Sound.BLOCK_TRIPWIRE_CLICK_ON.name());
                    final int animationSoundPitch = plugin.getConfig().getInt("settings.server.discoveries.discovered.title.animation_sound_pitch", 1);
                    if (current >= 1) {
                        final Sound sound = Sound.valueOf(animationSound.toUpperCase(Locale.ROOT));
                        player.playSound(player.getLocation(), sound, 1f, animationSoundPitch);
                    }
                }

                player.sendTitle(title,
                        subtitle,
                        fadein,
                        plugin.getConfig().getInt("settings.server.discoveries.discovered.title.stay"),
                        plugin.getConfig().getInt("settings.server.discoveries.discovered.title.fadeout"));

                current++;
            }
        }.runTaskTimer((JavaPlugin) plugin, 0L, speed);
    }
}
